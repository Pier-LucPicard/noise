
let Jimp = require('jimp')



function fade2(t)
{
    return t * t * (3 - 2 * t);
}


function fade(t) {
    return t*t*t*(t*(t*6-15)+10);
}



class Printer{

    constructor(image_size_x_px = 1000, image_size_y_py = 1000,  resolutionX, resolutionY, noise){
        this.image_size_x_px = image_size_x_px
        this.image_size_y_py = image_size_y_py
        this.image = new Jimp(image_size_x_px, image_size_y_py, () => {})
        this.setNoise( resolutionX, resolutionY, noise)
    }


    setNoise(resolutionX, resolutionY,noise){
        this.noise_map = new NoiseImageMap(this.image_size_x_px, this.image_size_y_py,  resolutionX, resolutionY, noise)
    }

    save(image_name){
        for (let x = 0; x < this.image_size_x_px; x++) {
            for (let y = 0; y < this.image_size_y_py; y++) {
                let color_i_normalize = this.noise_map.normalizeNoiseValue(x,y) * 255
               
                let color = Jimp.rgbaToInt(color_i_normalize, color_i_normalize, color_i_normalize, 255);
                this.image.setPixelColor(color,x,y) 
            }
        }
        this.image.write(image_name);
    }

}

class NoiseImageMap {
    constructor(size_x, size_y, resolutionX, resolutionY, noise) {
        this.size_x = size_x;
        this.size_y = size_y
        this.resolutionX = resolutionX
        this.resolutionY = resolutionY
        this.generate(noise)

    }
    generate(noise){
        this.noise_map = []
        this.min_noise_val = null;
        this.max_noise_val = null;
        for (let x = 0; x < this.size_x; x++) {
            for (let y = 0; y < this.size_y; y++) {
                let coordx = x*this.resolutionX;
                let coordy = y*this.resolutionY;
                const noiseAtPoint = noise.generateNoise(coordx, coordy)
                this.noise_map[x*this.size_x + y] = noiseAtPoint

                if(!this.min_noise_val){
                    this.min_noise_val = noiseAtPoint
                 }
          
                 if(!this.max_noise_val){
                    this.max_noise_val = noiseAtPoint
                 }
                 this.max_noise_val = this.max_noise_val > noiseAtPoint ? noiseAtPoint : this.max_noise_val
                 this.min_noise_val = this.min_noise_val < noiseAtPoint ? noiseAtPoint : this.min_noise_val
                 
            }
        }
    }

    noiseValue(x, y){
        return this.noise_map[x * this.size_x + y]
    }
    normalizeNoiseValue(x,y) {
        return (this.noise_map[x * this.size_x + y] - this.min_noise_val) / (this.max_noise_val - this.min_noise_val)
    }
}


class Perline2DNoise {
     static grad3 = [[1,1,0],[-1,1,0],[1,-1,0],[-1,-1,0],
        [1,0,1],[-1,0,1],[1,0,-1],[-1,0,-1],
        [0,1,1],[0,-1,1],[0,1,-1],[0,-1,-1]];

    static p = [151,160,137,91,90,15,
        131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
        190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
        88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
        77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
        102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
        135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
        5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
        223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
        129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
        251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
        49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
        138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180];

    constructor( PassedSeed, fade_function){
        this.perm = new Array(512);
        this.gradP = new Array(512);
        this.fade = fade_function;

        function seed(seed, _this) {
            if(seed > 0 && seed < 1) {
                // Scale the seed out
                seed *= 65536;
            }
        
            seed = Math.floor(seed);
            if(seed < 256) {
                seed |= seed << 8;
            }
        
            for(var i = 0; i < 256; i++) {
                var v;
                if (i & 1) {
                v = Perline2DNoise.p[i] ^ (seed & 255);
                } else {
                v = Perline2DNoise.p[i] ^ ((seed>>8) & 255);
                }
        
                _this.perm[i] = _this.perm[i + 256] = v;
                _this.gradP[i] = _this.gradP[i + 256] = Perline2DNoise.grad3[v % 12];
            }
        };
        seed(PassedSeed, this)
    }

    generateNoise(x,y){
        // Find unit grid cell containing point
        var X = Math.floor(x), Y = Math.floor(y);
         // Get relative xy coordinates of point within that cell
        x = x - X; y = y - Y;
        // Wrap the integer cells at 255 (smaller integer period can be introduced here)
        X = X & 255; Y = Y & 255;
        let ax = this.fade(x)
        let ay = this.fade(y);

        
        let h=[];
        let g=[];
        let v=[];
        for (let i = 0; i <= 1; i++) {
            h[i] = []
            g[i] = []
            v[i] = []
            for (let j = 0; j <= 1; j++) {
                h[i][j] = this.hash(X+i, Y+j)
                g[i][j] = this.gradP[h[i][j]]
          
                v[i][j] = (x-i)*g[i][j][0] + (y-j)*g[i][j][1]
            }
            
        }

        return Perline2DNoise.blend2(v, ax, ay)
    }

    static lerp(a, b, t) {
        return (1-t)*a + t*b;
    }
    hash(x,y){
        return x+this.perm[y]
    }
    static blend2(vals, ax, ay){
        const x0 = Perline2DNoise.lerp(vals[0][0], vals[1][0], ax)
        const x1 = Perline2DNoise.lerp(vals[0][1], vals[1][1], ax)
        const xy = Perline2DNoise.lerp(x0,x1, ay)
        return xy
    }
    
}
perline1 = new Perline2DNoise(3,fade)
perline2 = new Perline2DNoise(2345235,fade)
perline3 = new Perline2DNoise(34563456,fade)

perline4 = new Perline2DNoise(3,fade2)


const imagePrinter = new Printer(1000, 1000, 0.1,0.1, perline1)
imagePrinter.save('perline_noise_0_1_res.jpg');

imagePrinter.setNoise(0.2,0.2,perline1)
imagePrinter.save('perline_noise_0_2_res.jpg');

imagePrinter.setNoise(0.01,0.01,perline1)
imagePrinter.save('perline_noise_0_01_res.jpg');

imagePrinter.setNoise(0.001,0.001,perline1)
imagePrinter.save('perline_noise_0_001_res.jpg');

imagePrinter.setNoise(0.05,0.05,perline1)
imagePrinter.save('perline_noise_0_05_res.jpg');

imagePrinter.setNoise(0.5,0.5,perline1)
imagePrinter.save('perline_noise_0_5_res.jpg');

imagePrinter.setNoise(0.9,0.9,perline1)
imagePrinter.save('perline_noise_0_9_res.jpg');




imagePrinter.setNoise(0.01,0.01,perline1)
imagePrinter.save('perline_noise1_0_01_res.jpg');
imagePrinter.setNoise(0.01,0.01,perline2)
imagePrinter.save('perline_noise2_0_01_res.jpg');
imagePrinter.setNoise(0.01,0.01,perline3)
imagePrinter.save('perline_noise3_0_01_res.jpg');
imagePrinter.setNoise(0.01,0.01,perline4)
imagePrinter.save('perline_noise4_0_01_res.jpg');